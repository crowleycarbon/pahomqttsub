/** 
=====================
The MIT License (MIT)
=====================

Copyright (c) 2019 Crowley Carbon Ltd

Permission is hereby granted, free of charge, 
to any person obtaining a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, 
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

//
// Copyright (c) 2019, Crowley Carbon
// All Rights Reserved
//
// History:
//   02 Nov 19   johnmacenri   Creation
//

using haystack
using axon
using skyarcd
using folio

**
** PahoMqttSub library functions.
**
const class PahoMqttSubLib
{
  **
  ** Create a new **pahoMqttSubscriber** record and register it within the list of Subscribers being managed.
  **
  ** The 'brokerUrl' must be provided and must be a URI.
  **
  ** The 'topics' must be provided and must be a List of Str.
  **
  ** The 'parser' must be provided. This should match the name of an Axon function that takes two Str parameters: (topic, message).
  ** This parser will be used for all messages from all the topics listed.
  **
  ** The 'qos' is optional and will default to 1. See [MQTT Docs]`https://mosquitto.org/man/mqtt-7.html` for details on QOS.  
  **
  ** The 'clientId' is optional and will default to a random UUID if not supplied.   
  **
  ** If 'username' is provided, then the optional second parameter for password is also expected. 
  ** These will be supplied to the broker on connection. 
  ** If you have a non-authenticating broker, just leave out username and drop the second parameter.
  **
  ** Examples:
  **   pahoMqttSubscriberAdd({brokerUrl:`tcp://host:1883`, topics:["topic/one","topic/two"], parser:"myMsgParser", qos:1})
  **   pahoMqttSubscriberAdd({brokerUrl:`tcp://host:1883`, topics:["topic/one","topic/two"], parser:"myMsgParser", clientId:"myClientId", username:"username"}, "password")
  **
  ** Side effects:
  **   - Adds a new record to folio with the marker tag pahoMqttSubscriber combined with the tags from the provided Dict
  **   - If a username and password are provided, the password will be stored in the [password db]`docSkySpark::Security#passwords` 
  **     keyed by the pahoMqttSubscriber folio record id.
  **    
  @Axon { admin = true } static Dict pahoMqttSubscriberAdd(Dict sub, Str? password := null)
  {
      return PahoMqttSubExt.cur.addSubscriber(sub, password)
  }

  **
  ** Start a Subscriber passing the Ref ID of the folio record corresponding to it.
  **
  ** The Ref provided must match a record found in folio.
  **
  ** Examples:
  **   pahoMqttSubscriberStart(@p:proj1:r:255481c1-42be3946)
  **
  ** Side effects:
  **   - Connects the Subscriber to the Broker at the URL specified in its folio record
  **   - Subscribes to all topics listed
  **   - Returns the record with the transient 'running' tag set to **true** if it succeeded in starting
  **      
  @Axon { admin = true } static Dict pahoMqttSubscriberStart(Ref ref)
  {
      return PahoMqttSubExt.cur.startSubscriber(ref)
  }     

  **
  ** Stop a Subscriber passing the Ref ID of the folio record corresponding to it.
  **
  ** The Ref provided must match a record found in folio.
  **
  ** Examples:
  **   pahoMqttSubscriberStop(@p:proj1:r:255481c1-42be3946)
  **
  ** Side effects:
  **   - Unsubscribes from all topics
  **   - Disconnects the Subscriber from the Broker
  **   - Returns the record with the transient 'running' tag set to **false** if it succeeded in stopping
  **      
  @Axon { admin = true } static Dict pahoMqttSubscriberStop(Ref ref)
  {
      return PahoMqttSubExt.cur.stopSubscriber(ref)
  }     

  **
  ** Delete a Subscriber passing the Ref ID of the folio record corresponding to it.
  **
  ** The Ref provided must match a record found in folio.
  **
  ** Examples:
  **   pahoMqttSubscriberDelete(@p:proj1:r:255481c1-42be3946)
  **
  ** Side effects:
  **   - Unsubscribes from all topics
  **   - Disconnects the Subscriber from the Broker
  **   - Deletes the record from folio
  **      
  @Axon { admin = true } static Void pahoMqttSubscriberDelete(Ref ref)
  {
      PahoMqttSubExt.cur.deleteSubscriber(ref)
  }     

  **
  ** Stop all Subscribers.
  **
  ** Examples:
  **   pahoMqttSubscriberStopAll()
  **
  ** Side effects:
  **   - All Subscribers stopped
  **   - Returns the list of subscribers
  **      
  @Axon { admin = true } static Grid pahoMqttSubscriberStopAll()
  {
      return PahoMqttSubExt.cur.stopAllSubscribers()
  }     

  **
  ** Start all Subscribers.
  **
  ** Examples:
  **   pahoMqttSubscriberStartAll()
  **
  ** Side effects:
  **   - All Subscribers started
  **   - Returns the list of subscribers
  **      
  @Axon { admin = true } static Grid pahoMqttSubscriberStartAll()
  {
      return PahoMqttSubExt.cur.startAllSubscribers()
  }     


  **
  ** Create a new **pahoMqttPublisher** instance which can be used to publish multiple messages.
  ** It is the responsibility of the caller to close this publisher when publishing is complete, using the pahoMqttPublisherClose call.
  **
  ** The 'brokerUrl' must be provided and can be a Uri or a Str.
  **
  ** If 'username' is provided, then 'password' is also expected.
  **
  ** The 'cleanSession' is optional and will default to false.
  **
  ** The 'retained' is optional and will default to false.
  **
  ** The 'qos' is optional and will default to 1.
  **
  ** The 'clientId' is optional and will default to a random UUID if not supplied.
  **
  **
  ** Examples:
  **   pahoMqttPublisher({brokerUrl:`tcp://host:1883`, username:"username", password:"password", qos:1, retained:true, clientId:"myClientId"})
  **
  ** Side effects:
  **   - Creates a new Paho MQTT Client instance, connected and ready to publish messages.
  **
  @Axon { admin = true} static Publisher pahoMqttPublisher(Dict cfg)
  {
    return Publisher(cfg);
  }


  **
  ** Publish a single Str message to a topic using a previously constructed pahoMqttPublisher instance
  **
  ** The Ref provided must match a record found in folio.
  **
  ** Examples:
  **   pahoMqttPublisher({brokerUrl:`tcp://host:1883`, username:"username", password:"password", qos:1, retained:true, clientId:"myClientId"}).pahoMqttPublish("myMessage","myTopic")
  **
  ** Side effects:
  **   - The message is published to the topic chosen
  **
  @Axon { admin = true} static Void pahoMqttPublish(Publisher publisher, Str message, Str topic)
  {
    publisher.publish(message, topic);
  }

  **
  ** Close a previously constructed pahoMqttPublisher instance
  **
  ** Examples:
  **   pahoMqttPublisher({brokerUrl:`tcp://host:1883`, username:"username", password:"password", qos:1, retained:true, clientId:"myClientId"}).pahoMqttPublisherClose()
  **
  ** Side effects:
  **   - The pahoMqttPublisher instance is closed and can no longer be used to publish messages
  **
  @Axon { admin = true} static Void pahoMqttPublisherClose(Publisher publisher)
  {
    publisher.shutdown();
  }

}