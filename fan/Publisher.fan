/**
=====================
The MIT License (MIT)
=====================

Copyright (c) 2019 Crowley Carbon Ltd

Permission is hereby granted, free of charge,
to any person obtaining a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

//
// Copyright (c) 2019, Crowley Carbon
// All Rights Reserved
//
// History:
//   22 Nov 19   johnmacenri   Creation
//

using haystack
using axon
using skyarcd

using [java]org.eclipse.paho.client.mqttv3.persist::MemoryPersistence
using [java]org.eclipse.paho.client.mqttv3::MqttClient
using [java]org.eclipse.paho.client.mqttv3::MqttConnectOptions
using [java]org.eclipse.paho.client.mqttv3::MqttMessage

using [java]java.util::Properties

class Publisher
{
    MqttClient? client
    Int qos := 1
    Bool retained := false

    new make(Dict cfg)
    {
        Properties sslProps := Properties();
        sslProps.setProperty("com.ibm.ssl.protocol", "TLSv1.2");

        connOpts := MqttConnectOptions()
        if (cfg->brokerUrl.toStr.startsWith("ssl")) connOpts.setSSLProperties(sslProps)

        connOpts.setMqttVersion(MqttConnectOptions.MQTT_VERSION_3_1_1);
        if (cfg.has("username")) connOpts.setUserName(cfg->username)
        if (cfg.has("password")) connOpts.setPassword(ExtUtils.toCharArray(cfg->password))
        if (cfg.has("cleanSession")) connOpts.setCleanSession(cfg->cleanSession)

        clientId := Uuid().toStr
        if (cfg.has("clientId")) clientId = cfg->clientId

        if (cfg.has("qos")) qos = (cfg->qos as Number).toInt
        if (cfg.has("retained")) retained = cfg->retained

        client = MqttClient(cfg->brokerUrl.toStr, clientId, MemoryPersistence())
        client.connect(connOpts)

    }

    public Void publish(Str message, Str topic)
    {
        client.publish(topic, ExtUtils.toByteArray(message), qos, retained)
    }

    public Void shutdown()
    {
        client.disconnect()
        client.close()
    }

}