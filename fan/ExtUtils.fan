/**
=====================
The MIT License (MIT)
=====================

Copyright (c) 2019 Crowley Carbon Ltd

Permission is hereby granted, free of charge,
to any person obtaining a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

//
// Copyright (c) 2019, Crowley Carbon
// All Rights Reserved
//
// History:
//   02 Nov 19   johnmacenri   Creation
//

using [java] fanx.interop::Interop
using [java] fanx.interop::ByteArray
using [java] fanx.interop::CharArray

class ExtUtils
{

  static CharArray toCharArray(Str str)
  {
    chars := CharArray.make(str.size)
    str.chars.each |val, idx| { chars.set(idx, val)  }
    return chars
  }

  static ByteArray toByteArray(Str str)
  {
    return Interop.toJava(str.toBuf.trim).array
  }

}
