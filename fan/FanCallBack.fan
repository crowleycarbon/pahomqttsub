/** 
=====================
The MIT License (MIT)
=====================

Copyright (c) 2019 Crowley Carbon Ltd

Permission is hereby granted, free of charge, 
to any person obtaining a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, 
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

//
// Copyright (c) 2019, Crowley Carbon
// All Rights Reserved
//
// History:
//   02 Nov 19   johnmacenri   Creation
//

using [java] org.eclipse.paho.client.mqttv3::MqttCallbackExtended
using [java] org.eclipse.paho.client.mqttv3::MqttMessage
using [java] org.eclipse.paho.client.mqttv3::IMqttDeliveryToken
using [java] java.lang::Throwable
using [java] java.nio::ByteBuffer
using [java] fanx.interop::ByteArray
using [java] fanx.interop::Interop
using [java] java.lang::StackTraceElement


class FanCallBack : MqttCallbackExtended
{
  const static Log log := Log.get("FanCallBack")	
  
  new make(Subscriber s)
  {
    sub = s
  }
  
  override Void connectionLost(Throwable? cause) 
  {
    err := ""
    if (cause != null) err = cause.getMessage + "\n" + getStackTrace(cause)     
    log.warn("connectionLost. Error is $err")    
    //sub.connect
    //log.info("Reconnected")
  }
  
  override Void messageArrived(Str? topic, MqttMessage? message) 
  {
	log.debug("messageArrived")
    buf := byteArrayToBuf(message.getPayload)  
    Str msg := buf.readAllStr(true)
    log.debug("Message is\n$msg")
    try
    {
        sub.handleMessage(topic, msg)        
    }
    catch(Err e)
    {
        log.err("Error sending message to handler", e)
    }
  }

  override Void deliveryComplete(IMqttDeliveryToken? token) 
  {
    log.debug("deliveryComplete called")    
  }  

  override Void connectComplete(Bool reconnect, Str? serverUri)
  {
      log.info("Connection Complete.")
      if (reconnect) sub.subscribeAllTopics()
  }

  private Buf byteArrayToBuf(ByteArray bytes)
  {
    return Interop.toFan(ByteBuffer.wrap(bytes))
  }  
  
  private Str getStackTrace(Throwable t)
  {
    sb := StrBuf.make
    
    StackTraceElement[] elem := t.getStackTrace
    for (i := 0; i < elem.size; ++i)
    {
        sb.add(elem[i].toString + "\n")
    }
    return sb.toStr
  }  

//////////////////////////////////////////////////////////////////////////
// Fields
//////////////////////////////////////////////////////////////////////////
  Subscriber sub	
}
