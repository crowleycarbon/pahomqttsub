#! /usr/bin/env fan
/** 
=====================
The MIT License (MIT)
=====================

Copyright (c) 2019 Crowley Carbon Ltd

Permission is hereby granted, free of charge, 
to any person obtaining a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, 
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

//
// Copyright (c) 2019, Crowley Carbon
// All Rights Reserved
//
// History:
//   02 Nov 19   johnmacenri   Creation
//

using build
//using stackhubExt

**
** Build: pahoMqttSub
**
class Build : BuildPod
{
  new make()
  {
    podName = "pahoMqttSub"
    summary = "Paho MQTT Subscriber"
    meta    = ["org.name":       "Crowley Carbon",
               "org.uri":        "http://www.crowleycarbon.com/",
               "proj.name":      "pahoMqttSub",
               "license.name":   "MIT",
               "skyspark.docExt": "true"             
               ]
    depends  = ["sys 1.0",
                "concurrent 1.0",                
                "axon 3.1",
                "folio 3.1",
                "haystack 3.1",
                "pahoMqtt 1.2.2",
                "skyarc 3.1",
                "skyarcd 3.1",
               ]
    srcDirs = [`fan/`]
    resDirs = [`lib/`, `res/img/`]            
    docApi  = true
    version = Version("1.0.7")
    index =
    [
      "skyarc.ext": "pahoMqttSub::PahoMqttSubExt",
      "skyarc.lib": "pahoMqttSub::PahoMqttSubLib",
    ]
  }      
}

