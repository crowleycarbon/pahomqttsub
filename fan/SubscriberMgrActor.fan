/** 
=====================
The MIT License (MIT)
=====================

Copyright (c) 2019 Crowley Carbon Ltd

Permission is hereby granted, free of charge, 
to any person obtaining a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, 
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

//
// Copyright (c) 2019, Crowley Carbon
// All Rights Reserved
//
// History:
//   02 Nov 19   johnmacenri   Creation
//

using concurrent

const class SubscriberMgrActor : Actor
{ 
  const static Log log := Log.get("SubscriberMgrActor")
          
  new make(ActorPool p, PahoMqttSubExt ext) : super(p)
  { 
    this.ext = ext 
  }  
  const PahoMqttSubExt ext
  
  override Obj? receive(Obj? m)
  {
    mgr := Actor.locals["pahoMqttSub.mgr"] as SubscriberMgr
    if (mgr == null) Actor.locals["pahoMqttSub.mgr"] = mgr = SubscriberMgr(ext)

     msg := (Msg)m
     switch (msg.type)
     {
       case "add": return mgr.addSubscriber(msg.ref, msg.data)
       case "start": return mgr.startSubscriber(msg.ref, msg.data)
       case "stop": return mgr.stopSubscriber(msg.ref)
       case "delete": return mgr.deleteSubscriber(msg.ref)
       default: throw Err("Unhandled msg type: $msg.type")
     }
  }
}