/** 
=====================
The MIT License (MIT)
=====================

Copyright (c) 2019 Crowley Carbon Ltd

Permission is hereby granted, free of charge, 
to any person obtaining a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, 
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

//
// Copyright (c) 2019, Crowley Carbon
// All Rights Reserved
//
// History:
//   02 Nov 19   johnmacenri   Creation
//

using [java] org.eclipse.paho.client.mqttv3.persist::MemoryPersistence
using [java] org.eclipse.paho.client.mqttv3::MqttClient
using [java] org.eclipse.paho.client.mqttv3::MqttConnectOptions
using [java] org.eclipse.paho.client.mqttv3::MqttMessage
using [java] fanx.interop::CharArray
using skyarc
using skyarcd
using haystack

class Subscriber 
{
  const static Log log := Log.get("Subscriber")

  new make(PahoMqttSubExt ext, Dict sub) 
  { 
    this.ext = ext
            
    clientId := Uuid().toStr
    if (sub.has("clientId")) clientId = sub["clientId"]
            
    this.client = MqttClient(sub->brokerUrl.toStr, clientId, MemoryPersistence())
    cb := FanCallBack(this)
    this.client.setCallback(cb)    
    this.topics = sub->topics
    this.parser = sub->parser  
    
    if (sub.has("username")) this.username = sub["username"]
    if (sub.has("qos")) this.qos = sub["qos"]
  }  
  const PahoMqttSubExt ext
  const Str? username
  const Str[] topics
  const Int qos := 1
  const Str parser
  MqttClient client
  	
  public Void connect(Str? password := null)
  {      
	if (client.isConnected) return
		
    log.info("Connect subscriber with clientId " + client.getClientId())
    connOpts := MqttConnectOptions()

    connOpts.setCleanSession(true)  
    if (username != null && password != null)
    {
      connOpts.setUserName(username)
      connOpts.setPassword(ExtUtils.toCharArray(password))
    }    
    connOpts.setConnectionTimeout(5)
    connOpts.setKeepAliveInterval(5)
    connOpts.setAutomaticReconnect(true)
	connOpts.setMqttVersion(MqttConnectOptions.MQTT_VERSION_3_1_1);

	try
	{
		client.connect(connOpts)	
	    topics.each |topic| 
	    { 
	      log.info("subscribing to topic [$topic]")    
	      client.subscribe(topic, qos)
	    }		
	} catch (Err e) { 
		log.err("Error", e) 
		//throw e
	}
  }

  public Void subscribeAllTopics()
  {
    topics.each |topic|
    {
      log.info("subscribing to topic [$topic]")
      client.subscribe(topic, qos)
    }

  }

  public Void disconnect()
  {
    log.info("Disconnect subscriber with clientId " + client.getClientId())		

	if (!client.isConnected) return
		
	try
	{
        topics.each |topic| 
        { 
          log.info("unsubscribing from topic [$topic]")    
          client.unsubscribe(topic)
        }       	    
		client.disconnect()
	} catch (Err e) { 
		log.err("Error", e) 
		//throw e
	}
  }
  
  Void handleMessage(Str topic, Str msg)
  {    
      log.debug("Message received from topic $topic" )          
      ctx := Context(ext.proj.sys, User.conn, ext.proj) 
      ctx.call(parser, [topic, msg])      
  }
	
}
