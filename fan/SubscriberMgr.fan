/** 
=====================
The MIT License (MIT)
=====================

Copyright (c) 2019 Crowley Carbon Ltd

Permission is hereby granted, free of charge, 
to any person obtaining a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, 
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

//
// Copyright (c) 2019, Crowley Carbon
// All Rights Reserved
//
// History:
//   02 Nov 19   johnmacenri   Creation
//

using haystack

class SubscriberMgr
{
  const static Log log := Log.get("SubscriberMgr")
          
  new make(PahoMqttSubExt ext) 
  { 
    this.ext = ext 
  }  
  const PahoMqttSubExt ext
    
  [Ref:Subscriber] subs := [:]          
          
  Ref addSubscriber(Ref r, Dict d) 
  {
      sub := Subscriber(ext, d)
      subs.add(r, sub)
      return r
  }

  Ref startSubscriber(Ref r, Dict d)
  {
      subs[r].connect(d["password"])
      return r
  }

  Ref stopSubscriber(Ref r)
  {
      subs[r].disconnect
      return r
  }

  Int deleteSubscriber(Ref r)
  {
      subs[r].disconnect
      subs.remove(r)      
      return subs.size
  }

}
