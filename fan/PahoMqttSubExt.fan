/** 
=====================
The MIT License (MIT)
=====================

Copyright (c) 2019 Crowley Carbon Ltd

Permission is hereby granted, free of charge, 
to any person obtaining a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, 
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

//
// Copyright (c) 2019, Crowley Carbon
// All Rights Reserved
//
// History:
//   02 Nov 19   johnmacenri   Creation
//

using haystack
using skyarcd
using skyarc
using concurrent
using folio

@ExtMeta
{
  name    = "pahoMqttSub"
}
const class PahoMqttSubExt : Ext
{

  const SubscriberMgrActor actor := SubscriberMgrActor(proj.extActorPool, this)

  **
  ** Get PahoMqttSubExt for the current context.
  **
  static PahoMqttSubExt? cur(Bool checked := true)
  {
    Context.cur.ext("pahoMqttSub", checked)
  }
	
  @NoDoc new make( ) : super() { }
  
  override Void onStart()
  {
    log.info("onStart")    
    proj.readAll("pahoMqttSubscriber").each |rec| 
    {   
      actor.send(Msg("add", rec->id, rec))  
      startSubscriber(rec->id)
    } 
  }

  override Void onStop()
  {
    log.info("onStop")
    stopAllSubscribers()
  }  

  Dict addSubscriber(Dict sd, Str? password := null)
  {
    log.info("Add Subscriber")
    
    if (sd.has("id")) throw Err("You cannot supply an id when adding a Subscriber")
    
    mqttBroker := sd["brokerUrl"] ?: throw Err("Missing 'brokerUrl' tag")
    mqttBrokerUri := mqttBroker as Uri ?: throw Err("Type of 'brokerUrl' must be Uri, not $mqttBroker.typeof.name")

    topics := sd["topics"] ?: throw Err("Missing 'topics' tag")
    topicsList := topics as Str[] ?: throw Err("Type of 'topics' must be Str[], not $topics.typeof.name")

    parser := sd["parser"] ?: throw Err("Missing 'parser' tag")
    parserStr := parser as Str ?: throw Err("Type of 'parser' must be Str, not $parser.typeof.name")
            
    if (sd.has("username") && password == null) throw Err("You must supply a password parameter when username is supplied.")    
    
    sd = Etc.makeDict(Etc.dictToMap(sd).set("pahoMqttSubscriber", Marker.val))
    rec := proj.commit(Diff.makeAdd(sd))
    if (password != null) proj.passwords.set(rec->id.toStr, password)
    actor.send(Msg("add", rec->id, sd))  
    proj.commit(Diff.make(proj.readById(rec->id), ["running":false], Diff.transient))
    return proj.readById(rec->id)
  }
  
  Dict startSubscriber(Ref ref)
  { 
      log.info("Start Subscriber")
      d := Etc.emptyDict()
      Str? password := proj.passwords.get(ref.toStr)
      if (password != null) d = Etc.makeDict(["password":password])
      actor.send(Msg("start", ref, d)) 
      Dict rec := proj.readById(ref)
      proj.commit(Diff.make(rec, ["running":true], Diff.transient))
      return proj.readById(rec->id)      
  }

  Dict stopSubscriber(Ref ref)
  {  
      log.info("Stop Subscriber")
      actor.send(Msg("stop", ref)) 
      Dict rec := proj.readById(ref)
      proj.commit(Diff.make(rec, ["running":false], Diff.transient))
      return proj.readById(rec->id)      
  }  
  
  Void deleteSubscriber(Ref ref)
  {  
      log.info("Delete Subscriber")
      actor.send(Msg("delete", ref)) 
      Dict rec := proj.readById(ref)
      proj.commit(Diff.make(rec, null, Diff.remove))
  }

  Grid startAllSubscribers()
  {  
      log.info("Start All Subscribers")
      proj.readAll("pahoMqttSubscriber").each |rec| 
      {   
        startSubscriber(rec->id)
      } 
      return proj.readAll("pahoMqttSubscriber")
  }   
  
  Grid stopAllSubscribers()
  {  
      log.info("Stop All Subscribers")
      proj.readAll("pahoMqttSubscriber").each |rec| 
      {   
        stopSubscriber(rec->id)
      } 
      return proj.readAll("pahoMqttSubscriber")
  }
  
}